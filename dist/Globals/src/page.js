'use strict'

const XHR = require('core/utils/xhr_async.js');

var html = require('./page.html');

module.exports = class {
  constructor(cfg, socket) {
    this.element = document.createElement('div');
    this.element.innerHTML = html;
    this.name = cfg.name;
    this.parent_list = cfg.parent_list;
    this.filename = cfg.file;
    this.path = cfg.path;
    this.watch = cfg.watch;
    this.dirname = cfg.dirname;
    this.list_dirname = cfg.list_dirname;
    this.socket = socket;

    this.fname = this.path;
/*
    var max_length = 16;
    if(this.fname.length > max_length) {
      this.fname = this.fname.substring(0,max_length)+'...';
    }*/

    this.display();
  }

  display() {
    var page_element = this.element;
    page_element.classList.add('pages_ui_item');

    let this_class = this;

    var link = page_element.querySelector('h4');
    span_lines(link, this.path, 16);


    var path_p = page_element.querySelector('p');

    span_lines(path_p, this.dirname, 17);


    let _this = this;

    let less_watch_div = page_element.querySelector('div.watch_options div.watch_toggle:first-child');
    if (this.watch.less) less_watch_div.classList.add("watch_on");
    less_watch_div.addEventListener("click", function(e) {
      _this.socket.emit("toggle_watch_state_globals", {
        what: "less",
        id: {
          list: _this.list_dirname,
          dirname: _this.dirname
        }
      });
    });

    let webpack_watch_div = page_element.querySelector('div.watch_options div.watch_toggle:last-child');
    if (this.watch.webpack) webpack_watch_div.classList.add("watch_on");
    webpack_watch_div.addEventListener("click", function(e) {
      _this.socket.emit("toggle_watch_state_globals", {
        what: "webpack",
        id: {
          list: _this.list_dirname,
          dirname: _this.dirname
        }
      });
    });

    this.watch_divs = {
      less: less_watch_div,
      webpack: webpack_watch_div
    }

/*
    let webpack_btn = page_element.querySelector('.webpack_btn');
    webpack_btn.src = "webpack-off.png";

    if (this.watching) {
      webpack_btn.src = "webpack-on.png";
    }

    webpack_btn.addEventListener('click', async function(e) {
      const state = await XHR.post("/mellisuga/pages.io", {
        command: "webpack-watch",
        list: this_class.parent_list,
        name: this_class.name
      }, 'access_token');

      if (state) {
        webpack_btn.src = "webpack-on.png";
      } else {
        webpack_btn.src = "webpack-off.png";
      }
    });
*/
  }

  update_watch_state(what, state) {
    let watch_div = this.watch_divs[what];
    if (state) {
      watch_div.classList.add("watch_on");
    } else {
      watch_div.classList.remove("watch_on");
    }
  }
}


    function span_lines(p_element, pathname, max_length) {
      let path_array = pathname.split('/');

      while (path_array.includes('')) {
        path_array.splice(path_array.indexOf(''), 1);
      }

      function most_path_in_line() {
        let new_line = "";
        let spin = true;
        while (spin && new_line.length < max_length) {
          if (path_array.length > 0) {

            if (new_line.length+path_array[0].length > max_length) {
              if (path_array[0].length > max_length && new_line.length == 0) {
                new_line += "/"+path_array[0];
                path_array.shift();
              } else {
                spin = false;
              }
            } else {
              new_line += "/"+path_array[0];
              path_array.shift();
            }

            console.log(new_line);
          } else {
            spin = false;
          }
        }

        return new_line;
      }

      let spanef = document.createElement('span');
      spanef.innerHTML = most_path_in_line();
      p_element.appendChild(spanef);

      while (path_array.length > 0) {
        let spane = document.createElement('span');
        spane.innerHTML = most_path_in_line();
        p_element.appendChild(spane);
      }
    }
