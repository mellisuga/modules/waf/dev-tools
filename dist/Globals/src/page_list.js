'use strict'

const XHR = require('core/utils/xhr_async.js');
const GridUI = require('core/grid.ui/index.js');
const grid_row_length = 6;
const grid_padding = 20;
const grid_min_item_size = 150;

const Page = require('./page.js');


module.exports = class {
  constructor(name, page_list_data, socket) {
    let list = page_list_data.pages;

    this.dirname = page_list_data.dirname;

    this.element = document.createElement("div");
    this.element.classList.add('pages_ui');

    let list_flex = document.createElement("div");
    list_flex.classList.add("list_flex");
    this.element.appendChild(list_flex);

    this.page_list = [];
    for (let t = 0; t < list.length; t++) {
      list[t].list_dirname = this.dirname;
      var page = new Page(list[t], socket);
      list_flex.appendChild(page.element);
      this.page_list.push(page);
    }
  }

  update_watch_state(data) {
    for (let p = 0; p < this.page_list.length; p++) {
      if (data.id.dirname == this.page_list[p].dirname) {
        this.page_list[p].update_watch_state(data.what, data.state);
      }
    }
  }
}

function getWindowWidth() {
  let availWidth = window.screen.width * window.devicePixelRatio;
  if (window.innerWidth < availWidth) {
    return window.innerWidth;
  } else {
    return availWidth;
  }
}
