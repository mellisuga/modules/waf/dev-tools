const XHR = require('core/utils/xhr_async.js');

const GridUI = require('core/grid.ui/index.js');
const Page = require('./page.js');
const PageList = require('./page_list.js');


const socket_io_client = require('socket.io-client');

(async function() {
  try {

    let socket = socket_io_client(server_addr, {
      path: '/pagepack-socket.io',
      transportOptions: {
        polling: {
          extraHeaders: {
            "Authorization": AUTHENTIALS
          }
        }
      }
    });

    FLYAUTH_ONCHANGE(function() {
      socket.io.opts.transportOptions.polling.extraHeaders.Authorization = AUTHENTIALS;
    });

    console.log("CONNECTING TO", server_addr+'/pagepack-socket.io');
    socket.on('connect', function(){
      console.log("CONNECTED");
    });



    socket.on('error', (error) => {
      console.error(new Error(error.status+" "+error.msg));
    });

    socket.on('disconnect', function(){
      console.log("DISCONNETED");
    });


    socket.emit("test-req", "data");

//    await require("globals/header.js").construct();

    let div = document.createElement('div');
    document.body.appendChild(div);


    let page_list_data = await XHR.post('/mellisuga/pages.io', {
    /*  command: "select",
      method: "all_from_list",
      list: "pages-test"*/
      command: "select_pages",
      method: "all"
    }, "access_token");

    console.log("Pages", page_list_data);


    let page_lists = [];
    for (let key in page_list_data) {
      let page_list = new PageList(key, page_list_data[key], socket);
      div.appendChild(page_list.element);
      page_lists.push(page_list);
    }

    socket.on('update_watch_state', function(data) {
      for (let l = 0; l < page_lists.length; l++) {
        if (page_lists[l].dirname == data.id.list || (!page_lists[l].dirname && !data.id.list)) {
          page_lists[l].update_watch_state(data);
        }
      }
    });

  } catch (e) {
    console.error(e.stack);
  }
})();

