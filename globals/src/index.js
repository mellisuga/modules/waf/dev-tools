
const Flyauth = require("core/flyauth.ui/index.js");

(async function() {
  try {
    await Flyauth.prepare("/admin_auth");

  } catch (e) {
    console.error(e.stack);
  }
})();
