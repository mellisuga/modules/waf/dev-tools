
const fs = require('fs');
const path = require('path');

const Aura = require('pg-aura');
//const Aura = require('./pg-aura/index.js');

const Accounts = require('./accounts/index.js');

const crypto = require("crypto");
const bcrypt = require("bcryptjs");

const express = require("express");


module.exports = class {
  constructor(cfg) {
    this.super_disabled = cfg.super_disabled;
  }

  static async init(waf, cfg) {
    let cmbird = waf;
    let this_class = new module.exports(cfg);
    let config = cmbird.config;



    cfg.admin_mode = true;
    cfg.table_name = "admin_accounts";

    cfg.on_auth = async function(account) {
      if (cmbird.mailer && account.cfg && account.cfg.email) {
        await cmbird.mailer.session(account.cfg.emails, account.id);
      }
    }


    let flyadmin = waf.modules.get_module("flyadmin");
    let sess_accs = await flyadmin.auth.all_sessions();
    for (let a = 0; a < sess_accs.length; a++) {
      if (cmbird.mailer && sess_accs[a].cfg && sess_accs[a].cfg.emails) {
        await cmbird.mailer.session(sess_accs[a].cfg.emails, sess_accs[a].id);
      }
    }

    let accounts = this_class.accounts = await Accounts.init(cmbird);

    this_class.table = flyadmin.auth.table;
    

//    cmbird.pages.serve_dir("/cmbird_admin", path.resolve(__dirname, 'dist-bak'), auth);
    if (!this_class.super_disabled) {
      cmbird.pages.serve_dirs("/dev-tools", path.resolve(__dirname, 'dist'), {
        auth: flyadmin.auth,
        globals_path: path.resolve(__dirname, 'globals'),
        name: "admin",
        dev_only: true,
        required_rights: [
          'super_admin'
        ],
        "root_page": "Pages",
        aliases: {
          globals: path.resolve(__dirname, "globals/modules")
        }
      });
    }

    if (!fs.existsSync(cmbird.globals_path)){
      fs.mkdirSync(cmbird.globals_path);
    }

    waf.pages.init_controls(flyadmin.auth, waf);
    return this_class;
  }
}
