const bcrypt = require('bcryptjs');

const { Authenticator } = require('@otplib/core');
const { createDigest, createRandomBytes } = require('@otplib/plugin-crypto'); // use your chosen crypto plugin
const { keyDecoder, keyEncoder } = require('@otplib/plugin-thirty-two'); // use your chosen base32 plugin

const authenticator = new Authenticator({
  createDigest,
  createRandomBytes,
  keyDecoder,
  keyEncoder
});

module.exports = class {
  constructor(cmbird) {
    let flyadmin = cmbird.modules.get_module("flyadmin");
    let app = cmbird.app, table = flyadmin.auth.table;
    this.app = app;

    this.table = table;

    var this_class = this;
    let _this = this;

//    console.log("ADMIN AUTH PATH ", cmbird.config.admin_path+"/admin_accounts.io");
    app.post(cmbird.config.admin_path+"/admin_accounts.io", flyadmin.auth.orize_gen(["super_admin"]), async function(req, res) {
      try {
        let data;
        try {
          data = JSON.parse(req.body.data);
        } catch (e) {
          data = req.body.data;
        }

        var command = data.command;
        data = data.data
        /*
          {
            command: string -> "create"|"edit"|"delete",
            title: "string",
            post: {
              title: "string",
              tags: ["string"],
              content: "string"
            },
            ids: ["UUID"]
          }
        */
        switch (command) {
          case "all":
            var auras = await table.select(['email', 'super', 'cfg', 'creator', 'totp_secret']);
            for (let u = 0; u < auras.length; u++) {
              if (auras[u].totp_secret) auras[u].totp_enabled = true;
              delete auras[u].totp_secret;
            }
            res.send(JSON.stringify(auras));
            break;
          case "add":
            console.log("ADD ADMIN", data);

            const existing = await table.select(
              ["email"],
              "email = $1",
              [data.email]
            );

            if (existing.length > 0) {
              res.send("EMAIL ALREADY IN USE!");
            } else {
              var salt = bcrypt.genSaltSync(10);
              data.password = bcrypt.hashSync(data.password, salt);
              console.log(data);
              if (data.cfg === "") {
                data.cfg = "{}";
              }
              await table.insert(data);
              res.send("success");
            }
            break;
          case "rm":
          console.log("REMOVE", data.email);
            await table.delete(
              "email = $1",
              [data.email]
            );
            res.send("success");
            break;
          case "edit":
            try {
              JSON.parse(data.cfg);
              await table.update(
                { super: data.super, cfg: data.cfg },
                "email = $1",
                [data.email]
              );
              res.send("success");
            } catch (e) {
              res.send(e.message);
            }
            break;
          case "generate_totp":
            console.log(data);
            res.json(await _this.generate_totp(data.uid)); 
            break;
          default:
            console.error("Unknown command:", data.command);
        }
      } catch(e) {
        console.error(e.stack);
      };
    });

  }

  static async init(app, table) {
    try {

      return new module.exports(app, table);
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async generate_totp(uid) {
    try {
      let result_obj = {
        totp_secret: authenticator.generateSecret()
      }

      await this.table.update(result_obj, "email = $1", [uid]);

      return result_obj;
    } catch (e) {
      console.error(e.stack);
    }
  }
}
